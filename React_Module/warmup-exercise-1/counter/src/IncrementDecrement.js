import React from "react";

export default function IncrementDecrement(props) {
  return (
    <div>
      <button
        onClick={() => {
          props.incrementCounter();
        }}
        className="btn btn-danger mr-2"
      >
        +
      </button>
      <button
        onClick={() => {
          props.decrementCounter();
        }}
        className="btn btn-success mr-2"
      >
        -
      </button>
    </div>
  );
}
