import React, { useState } from "react";
import "./App.css";
import IncrementDecrement from "./IncrementDecrement";

function App() {
  const [counter, setCounter] = useState(0);
  function incrementCounter() {
    setCounter(counter + 1);
  }
  function decrementCounter() {
    setCounter(counter - 1);
  }
  return (
    <div className="container">
      {counter}
      <IncrementDecrement
        incrementCounter={incrementCounter}
        decrementCounter={decrementCounter}
      />
    </div>
  );
}

export default App;
