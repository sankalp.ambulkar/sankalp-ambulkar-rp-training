import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Img from "./containers/Img";
import Description from "./containers/Description";
import Title from "./containers/Title";
function App() {
  return (
    <div className="container">
      <div className="row border border-primary p-3">
        <div className="border border-warning col-3">
          <Img content="Img" />
        </div>
        <div className="col-6">
          <div className="border border-danger">
            <Title content="A title" />
          </div>
          <div className="border border-dark">
            <Description content="The Description goes here" />
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
