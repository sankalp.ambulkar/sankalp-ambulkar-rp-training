import React from "react";
import PropTypes from "prop-types";
import { Button } from "../Buttons/Button";
import { OperationButton } from "../Buttons/Button";
import { ClearButton } from "../Buttons/Button";
export default function Keypad(props) {
  return (
    <div className="w-100">
      <section>
        <KeypadRow>
          <ClearButton OnButtonPress={props.OnButtonPress}>clear</ClearButton>
          <OperationButton OnButtonPress={props.OnButtonPress}>
            /
          </OperationButton>
        </KeypadRow>
        <KeypadRow>
          <Button OnButtonPress={props.OnButtonPress}>7</Button>
          <Button OnButtonPress={props.OnButtonPress}>8</Button>
          <Button OnButtonPress={props.OnButtonPress}>9</Button>
          <OperationButton OnButtonPress={props.OnButtonPress}>
            +
          </OperationButton>
        </KeypadRow>
        <KeypadRow>
          <Button OnButtonPress={props.OnButtonPress}>4</Button>
          <Button OnButtonPress={props.OnButtonPress}>5</Button>
          <Button OnButtonPress={props.OnButtonPress}>6</Button>
          <OperationButton OnButtonPress={props.OnButtonPress}>
            -
          </OperationButton>
        </KeypadRow>
        <KeypadRow>
          <Button OnButtonPress={props.OnButtonPress}>1</Button>
          <Button OnButtonPress={props.OnButtonPress}>2</Button>
          <Button OnButtonPress={props.OnButtonPress}>3</Button>
          <OperationButton OnButtonPress={props.OnButtonPress}>
            *
          </OperationButton>
        </KeypadRow>
        <KeypadRow>
          <Button OnButtonPress={props.OnButtonPress}>0</Button>
          <Button OnButtonPress={props.OnButtonPress}>.</Button>
          <OperationButton OnButtonPress={props.OnButtonPress}>
            =
          </OperationButton>
        </KeypadRow>
      </section>
    </div>
  );
}
function KeypadRow(props) {
  return <div className=" w-100 text-black d-flex">{props.children}</div>;
}
Keypad.propTypes = {
  OnButtonPress: PropTypes.func,
};
