import React, { useState } from "react";
import OperationScreen from "./screen/OperationScreen";
import ResultScreen from "./screen/ResultScreen";
import Keypad from "./keypad/Keypad";
export default function Calculator() {
  const [equation, setEquation] = useState("");
  const [result, setResult] = useState(0);
  const OnButtonPress = (event) => {
    let expression = equation;
    let pressedButton = event.target.innerHTML;
    if (pressedButton === "clear") {
      setEquation("");
      setResult(0);
    } else if (
      (pressedButton >= 0 && pressedButton <= 9) ||
      pressedButton === "."
    )
      setEquation(equation + pressedButton);
    else if (
      pressedButton === "+" ||
      pressedButton === "-" ||
      pressedButton === "*" ||
      pressedButton === "/" ||
      pressedButton === "%"
    ) {
      setEquation(equation + " ");
      setEquation(equation + pressedButton);
    } else {
      try {
        expression = eval(expression);
        setResult(expression);
      } catch (error) {
        alert("Illegal expression");
      }
    }
  };
  return (
    <div className="w-25 bg-white border border-danger">
      Calculator
      <ResultScreen result={result} />
      <OperationScreen equation={equation} />
      <Keypad OnButtonPress={OnButtonPress} />
    </div>
  );
}
