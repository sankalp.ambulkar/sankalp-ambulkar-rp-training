import React from "react";
import PropTypes from "prop-types";

function Button(props) {
  return (
    <button
      onClick={(e) => {
        props.OnButtonPress(e);
      }}
      className="w-25 h-100 border border-danger d-flex justify-content-center align-items-center bg-white"
    >
      {props.children}
    </button>
  );
}
function OperationButton(props) {
  return (
    <button
      onClick={(e) => {
        props.OnButtonPress(e);
      }}
      className="w-25 h-100 border border-danger text-white bg-danger d-flex justify-content-center align-items-center "
    >
      {props.children}
    </button>
  );
}
function ClearButton(props) {
  return (
    <button
      onClick={(e) => {
        props.OnButtonPress(e);
      }}
      className="w-75 h-100 border border-danger bg-white d-flex justify-content-center align-items-center "
    >
      {props.children}
    </button>
  );
}
Button.propTypes = {
  OnButtonPress: PropTypes.func,
};
OperationButton.propTypes = {
  OnButtonPress: PropTypes.func,
};
ClearButton.propTypes = {
  OnButtonPress: PropTypes.func,
};
export { Button, OperationButton, ClearButton };
