import React from "react";
import PropTypes from "prop-types";
export default function ResultScreen(props) {
  return (
    <div className="d-flex  h3 bg-dark text-white border-danger justify-content-end m-0">
      {props.result}
    </div>
  );
}
ResultScreen.propTypes = {
  result: PropTypes.number,
};
