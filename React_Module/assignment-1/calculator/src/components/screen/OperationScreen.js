import React from "react";
import PropTypes from "prop-types";
export default function OperationScreen(props) {
  return (
    <div className="d-flex bg-dark text-white border-bottom border-danger justify-content-end">
      {props.equation}
    </div>
  );
}
OperationScreen.propTypes = {
  equation: PropTypes.string,
};
