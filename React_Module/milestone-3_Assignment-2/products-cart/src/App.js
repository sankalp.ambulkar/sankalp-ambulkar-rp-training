import React from "react";
import ProductCart from "./Components/ProductCart";
function App() {
  return (
    <div className="App">
      <ProductCart />
    </div>
  );
}

export default App;
