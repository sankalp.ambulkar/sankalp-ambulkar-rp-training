import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import {
  ProductCart,
  Seller,
  Products,
  Login,
  Signup,
} from "./Components/index";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import * as serviceWorker from "./serviceWorker";

const Home = () => {
  let loggedIn = localStorage.getItem("user");
  const doRedirect = () => {
    if (loggedIn) return <Redirect to="/"></Redirect>;
    else return <Redirect to="/signup"></Redirect>;
  };
  return (
    <Router>
      <Route exact path="/" component={App}></Route>
      <Route path="/login" component={Login}></Route>
      <Route path="/signup" component={Signup}></Route>
      <Route path="/productCart" component={ProductCart}></Route>
      <Route path="/seller" component={Seller}></Route>
      <Route path="/products" component={Products}></Route>
      {doRedirect()}
    </Router>
  );
};
ReactDOM.render(<Home />, document.getElementById("root"));
serviceWorker.unregister();
