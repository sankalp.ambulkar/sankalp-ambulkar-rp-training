import React, { useState } from "react";
import { useHistory } from "react-router-dom";
export default function Login() {
  const [state, setState] = useState({
    email: "",
    password: "",
    emailError: "",
    passwordError: "",
  });
  const history = useHistory();
  const handleOnChange = (event) => {
    setState({ [event.target.name]: event.target.value });
  };
  const validate = () => {
    let emailError = "";
    let passwordError = "";
    let temp = JSON.parse(localStorage.getItem("signup"));

    if (state.email !== temp.email) {
      emailError = "email do not match";
    }
    if (state.password !== temp.password2) {
      passwordError = "password do not match";
    }
    if (emailError || passwordError) {
      setState({
        emailError: emailError,
        passwordError: passwordError,
      });
      return false;
    }
    return true;
  };
  const handleOnSubmit = (event) => {
    event.preventDefault();
    const isValid = validate();
    if (isValid) {
      localStorage.setItem("user", JSON.stringify(state));
      history.push("/");
    }
  };
  return (
    <div>
      <form onSubmit={handleOnSubmit}>
        <label>
          Email
          <input onChange={handleOnChange} name="email" type="email"></input>
          <div style={{ color: "red" }}>{state.emailError}</div>
        </label>
        <label>
          Password
          <input
            onChange={handleOnChange}
            name="password"
            type="password"
          ></input>
          <div style={{ color: "red" }}>{state.passwordError}</div>
        </label>
        <button type="submit">Login</button>
      </form>
    </div>
  );
}
