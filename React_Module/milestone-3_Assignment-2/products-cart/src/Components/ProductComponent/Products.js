import React, { useState } from "react";
import Card from "../CardComponent";
import data from "../../data/sellers.json";
import { number } from "prop-types";

const propTypes = { id: number };

export default function Products({
  location: {
    state: { id },
  },
}) {
  const [cart, setCart] = useState(0);
  const cartIncrement = () => {
    setCart(cart + 1);
  };
  const products = data[id - 1].products;
  return (
    <div>
      <Card data={products} cartIncrement={cartIncrement} />
      Your cart:{cart}
    </div>
  );
}

Products.propTypes = propTypes;
