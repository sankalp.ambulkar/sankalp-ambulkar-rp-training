import React, { useState } from "react";
import { useHistory } from "react-router-dom";
let initialState = {
  firstName: "",
  lastName: "",
  email: "",
  password1: "",
  password2: "",
  passwordError: "",
};
export default function Signup() {
  const [state, setState] = useState(initialState);
  const history = useHistory();
  const handleOnChange = (event) => {
    setState({ [event.target.name]: event.target.value });
  };
  const validate = () => {
    var passwordError = "";

    if (state.password1 !== state.password2) {
      passwordError = "passwords do not match";
    }
    if (passwordError) {
      setState({
        passwordError: state.passwordError,
      });
      return false;
    }
    return true;
  };
  const handleOnSubmit = (event) => {
    event.preventDefault();
    let isValid = validate();
    if (isValid) {
      setState(initialState);
      localStorage.setItem("signup", JSON.stringify(state));
      history.push("/login");
    }
  };

  return (
    <div className="container">
      <div>
        <form onSubmit={handleOnSubmit}>
          <div>
            <label>
              Email
              <input
                name="email"
                placeholder="sankalp@gmail.com"
                onChange={handleOnChange}
                type="email"
              ></input>
              <div style={{ color: "red" }}>{state.emailError}</div>
            </label>
          </div>

          <div className="col-4">
            <label>
              Password
              <input
                name="password1"
                onChange={handleOnChange}
                type="password"
              ></input>
            </label>
          </div>
          <div>
            <label>
              Confirm Password
              <input
                name="password2"
                onChange={handleOnChange}
                type="password"
              ></input>
              <div style={{ color: "red" }}>{state.passwordError}</div>
            </label>
          </div>
          <div>
            <button type="submit">Signup</button>
          </div>
        </form>
      </div>
    </div>
  );
}
