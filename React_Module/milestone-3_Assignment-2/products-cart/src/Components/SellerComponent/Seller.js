import React, { useState, useEffect } from "react";
import data from "../../data/sellers.json";
import Card from "../CardComponent";

export default function Seller() {
  const [sellers, setSellers] = useState([]);

  useEffect(() => {
    setSellers(data);
  }, [sellers]);
  return <Card data={sellers} />;
}
