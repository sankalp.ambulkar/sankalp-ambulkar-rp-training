import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { ProductCart, Seller, Products } from "./Components/index";
import { BrowserRouter as Router, Route } from "react-router-dom";

import * as serviceWorker from "./serviceWorker";

const Home = () => {
  return (
    <Router>
      <Route exact path="/" component={App}></Route>
      <Route path="/productCart" component={ProductCart}></Route>
      <Route path="/seller" component={Seller}></Route>
      <Route path="/products" component={Products}></Route>
    </Router>
  );
};
ReactDOM.render(<Home />, document.getElementById("root"));
serviceWorker.unregister();
