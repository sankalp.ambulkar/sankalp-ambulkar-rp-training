const success = () => {
  return {
    type: "success",
  };
};
const error = () => {
  return {
    type: "error",
  };
};
const warning = () => {
  return {
    type: "warning",
  };
};

export { success, error, warning };
