import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";
import { Provider } from "react-redux";
import {
  ProductCart,
  Seller,
  Products,
  Login,
  Signup,
} from "./Components/index";
import { store } from "./redux/index";
import { useDispatch } from "react-redux";
import { warning } from "./redux/action";
import * as serviceWorker from "./serviceWorker";

const Home = () => {
  let loggedIn = localStorage.getItem("user");
  const dispatch = useDispatch();
  const doRedirect = () => {
    if (loggedIn) return <Redirect to="/"></Redirect>;
    else {
      dispatch(warning());
      return <Redirect to="/signup"></Redirect>;
    }
  };
  return (
    <Router>
      <Route exact path="/" component={App}></Route>
      <Route path="/login" component={Login}></Route>
      <Route path="/signup" component={Signup}></Route>
      <Route path="/productCart" component={ProductCart}></Route>
      <Route path="/seller" component={Seller}></Route>
      <Route path="/products" component={Products}></Route>
      {doRedirect()}
    </Router>
  );
};
ReactDOM.render(
  <Provider store={store}>
    <Home />
  </Provider>,
  document.getElementById("root")
);
serviceWorker.unregister();
