import React from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { success } from "../../redux/action";
import "./styles/main.css";
export default function Logout() {
  const history = useHistory();
  const dispatch = useDispatch();
  const handleOnClick = () => {
    localStorage.removeItem("user");
    dispatch(success());
    history.push("/login");
  };
  return (
    <button className="logout-button" onClick={handleOnClick}>
      Logout
    </button>
  );
}
