import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { error } from "../../redux/action";
import "./styles/main.css";
let initialState = {};
export default function Signup() {
  const [state, setState] = useState({
    email: "",
    password1: "",
    password2: "",
  });
  const history = useHistory();
  const dispatch = useDispatch();
  const handleOnChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.value });
  };
  const validate = () => {
    if (state.password1 !== state.password2) {
      dispatch(error());
      return false;
    }
    return true;
  };
  const handleOnSubmit = (event) => {
    event.preventDefault();
    let isValid = validate();
    if (isValid) {
      setState(initialState);
      localStorage.setItem("signup", JSON.stringify(state));
      history.push("/login");
    } else {
      history.push("/signup");
    }
  };

  return (
    <div className="signup-container">
      <form className="form" onSubmit={handleOnSubmit}>
        <div className="input-email">
          <label>
            Email
            <input
              name="email"
              placeholder="sankalp@gmail.com"
              onChange={handleOnChange}
              type="email"
            ></input>
          </label>
        </div>

        <div className="input-password1">
          <label>
            Password
            <input
              name="password1"
              onChange={handleOnChange}
              type="password"
            ></input>
          </label>
        </div>
        <div className="input-password2">
          <label>
            Confirm Password
            <input
              name="password2"
              onChange={handleOnChange}
              type="password"
            ></input>
          </label>
        </div>
        <div className="signup">
          <button className="signup-button" type="submit">
            Signup
          </button>
        </div>
      </form>
    </div>
  );
}
