import React from "react";
import { useHistory } from "react-router-dom";
import { arrayOf, object } from "prop-types";
import { func } from "prop-types";
import "./styles/main.css";
const propTypes = { data: arrayOf(object), cartIncrement: func };

export default function Card({ data, cartIncrement }) {
  const history = useHistory();
  const onSellerClickHandler = (event) => {
    history.push("/products", { id: event.target.value });
  };
  const onproductClickHandler = () => {
    cartIncrement();
  };
  return (
    <div className="card-container">
      {data.map(({ id, avatar, name, description, type }) => {
        return (
          <div className="card" key={id}>
            <img src={avatar} alt="Seller Avatar" />
            <div className="card-body">
              <h4>
                <b>{name}</b>
              </h4>
              <p>{description}</p>
              {type === "seller" ? (
                <button
                  className="view-products-button"
                  value={id}
                  onClick={onSellerClickHandler}
                >
                  Click to view products
                </button>
              ) : (
                <button
                  className="add-to-cart-button"
                  onClick={onproductClickHandler}
                >
                  Add to cart
                </button>
              )}
            </div>
          </div>
        );
      })}
    </div>
  );
}

Card.propTypes = propTypes;
