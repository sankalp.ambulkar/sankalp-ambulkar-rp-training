import React, { useState } from "react";
import Card from "../CardComponent";
import data from "../../data/sellers.json";
import "./styles/main.css";
import cartLogo from "./resources/059-cart.png";
import { useDispatch } from "react-redux";
import { success } from "../../redux/action";
import { number } from "prop-types";
import Logout from "../LogoutComponent/";
const propTypes = { id: number };

export default function Products({
  location: {
    state: { id },
  },
}) {
  const [cart, setCart] = useState(0);
  const dispatch = useDispatch();
  const cartIncrement = () => {
    dispatch(success());
    setCart(cart + 1);
  };
  const products = data[id - 1].products;
  return (
    <div>
      <div className="logout-component">
        <Logout />
      </div>
      <div className="products-container">
        <Card
          className="products"
          data={products}
          cartIncrement={cartIncrement}
        />
        <div className="cart">
          <img src={cartLogo} alt="cart-img" />
          <div>Your cart:{cart}</div>
        </div>
      </div>
    </div>
  );
}

Products.propTypes = propTypes;
