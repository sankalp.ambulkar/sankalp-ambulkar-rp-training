import React, { useState, useEffect } from "react";
import data from "../../data/sellers.json";
import Card from "../CardComponent";
import Logout from "../LogoutComponent";
import "./styles/main.css";
export default function Seller() {
  const [sellers, setSellers] = useState([]);

  useEffect(() => {
    setSellers(data);
  }, [sellers]);
  return (
    <div>
      <div className="logout-component">
        <Logout />
      </div>

      <div className="card-component">
        <Card data={sellers} />
      </div>
    </div>
  );
}
