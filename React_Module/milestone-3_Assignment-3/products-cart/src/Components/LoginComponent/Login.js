import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch } from "react-redux";
import { success } from "../../redux/action";
import { error } from "../../redux/action";
import "./styles/main.css";

export default function Login() {
  const [state, setState] = useState({
    email: "",
    password: "",
  });
  const history = useHistory();
  const dispatch = useDispatch();

  const handleOnChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.value });
  };
  const validate = () => {
    let temp = JSON.parse(localStorage.getItem("signup"));

    if (state.email !== temp.email) {
      dispatch(error());
      return false;
    }
    if (state.password !== temp.password2) {
      dispatch(error());
      return false;
    }
    return true;
  };
  const handleOnSubmit = (event) => {
    event.preventDefault();
    const isValid = validate();
    if (isValid) {
      localStorage.setItem("user", JSON.stringify(state));
      dispatch(success());
      history.push("/");
    }
  };
  return (
    <div className="login-container">
      <form className="form" onSubmit={handleOnSubmit}>
        <div className="input-email">
          <label>
            Email
            <input onChange={handleOnChange} name="email" type="email"></input>
            <div style={{ color: "red" }}>{state.emailError}</div>
          </label>
        </div>
        <div className="input-password">
          <label>
            Password
            <input
              onChange={handleOnChange}
              name="password"
              type="password"
            ></input>
            <div style={{ color: "red" }}>{state.passwordError}</div>
          </label>
        </div>
        <div className="login">
          <button className="login-button" type="submit">
            Login
          </button>
        </div>
      </form>
    </div>
  );
}
